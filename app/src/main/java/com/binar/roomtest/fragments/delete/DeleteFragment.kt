package com.binar.roomtest.fragments.delete

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.binar.roomtest.R
import com.binar.roomtest.databinding.FragmentDeleteBinding
import com.binar.roomtest.viewmodel.UserViewModel

class DeleteFragment : DialogFragment() {
    private var _binding: FragmentDeleteBinding? = null
    private val binding get() = _binding!!


    private val args by navArgs<DeleteFragmentArgs>()
    private lateinit var mUserViewModel: UserViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteBinding.inflate(inflater, container, false)
        val view = binding.root

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        binding.btnYa.setOnClickListener {
            deleteUser()
        }

        binding.btnTidak.setOnClickListener {
            Toast.makeText(requireContext(), "Data tidak dihapus", Toast.LENGTH_LONG).show()
            val action = DeleteFragmentDirections.actionDeleteFragment2ToListFragment()
            findNavController().navigate(action)
        }

        return view
    }

    private fun deleteUser(){
        mUserViewModel.deleteUser(args.deleteUser)
        Toast.makeText(requireContext(), "Data ${args.deleteUser.judul} berhasil dihapus", Toast.LENGTH_LONG).show()
        findNavController().navigate(R.id.action_deleteFragment2_to_listFragment)
    }
}