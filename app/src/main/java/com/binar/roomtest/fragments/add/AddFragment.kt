package com.binar.roomtest.fragments.add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.binar.roomtest.R
import com.binar.roomtest.model.User
import com.binar.roomtest.viewmodel.UserViewModel
import com.binar.roomtest.databinding.FragmentAddBinding

class AddFragment : DialogFragment() {
    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!

    private lateinit var mUserViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(inflater, container, false)
        val view = binding.root

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        binding.btnAdd.setOnClickListener {
            insertDataToDatabase()
        }

        return view
    }

    private fun insertDataToDatabase() {
        val judul = binding.editNama.text.toString()
        val catatan = binding.editEmail.text.toString()

        if(inputCheck(judul, catatan)){
            val user = User(0, judul, catatan)
            mUserViewModel.addUser(user)

            Toast.makeText(requireContext(), "Data berhasil ditambahkan", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }

        else{
            Toast.makeText(requireContext(), "Data belum diisi", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck (Judul: String, Catatan: String): Boolean{
        return !(TextUtils.isEmpty(Judul) && TextUtils.isEmpty(Catatan))
    }

}