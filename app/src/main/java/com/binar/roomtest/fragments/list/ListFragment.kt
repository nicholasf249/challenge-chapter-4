package com.binar.roomtest.fragments.list

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.binar.roomtest.LoginActivity
import com.binar.roomtest.R
import com.binar.roomtest.viewmodel.UserViewModel
import com.binar.roomtest.databinding.FragmentListBinding
import kotlinx.android.synthetic.main.fragment_list.view.*

class ListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    private val sharedPreferences = "LOGIN"

    private lateinit var mUserViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        val view = binding.root

        val sharedPreferences: SharedPreferences =
            requireActivity().getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)

        val user = sharedPreferences.getString("USER", "")

        binding.tvWelcome.text = "Welcome, $user!"

        val adapter = ListAdapter()
        val recyclerView = view.recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        mUserViewModel.readAllData.observe(viewLifecycleOwner, Observer { user ->
            adapter.setData(user)
            if(adapter.itemCount.equals(0)){
                binding.tvDataKosong.visibility = View.VISIBLE
            }
            else{
                binding.tvDataKosong.visibility = View.GONE
            }
        })

        binding.floatingActionButton.setOnClickListener{
            findNavController().navigate(R.id.action_listFragment_to_addFragment)
        }

        binding.tvLogout.setOnClickListener {
            AlertDialog.Builder(requireContext())
                .setTitle("Logout")
                .setMessage("Apakah anda ingin logout?")
                .setPositiveButton("Ya") { _, _ ->
                    val editor: SharedPreferences.Editor = sharedPreferences.edit()
                    editor.putBoolean("LOGIN", false)
                    editor.apply()

                    Toast.makeText(requireContext(), "Anda telah logout.", Toast.LENGTH_LONG).show()
                    val intent = Intent(activity, LoginActivity::class.java)
                    startActivity(intent)
                }
                .setNegativeButton("Tidak") { _, _ ->
                }
                .show()
        }

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(requireContext(), "Anda telah logout.", Toast.LENGTH_LONG).show()
        activity?.finish()
    }
}