package com.binar.roomtest.fragments.update

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.binar.roomtest.R
import com.binar.roomtest.databinding.FragmentUpdateBinding
import com.binar.roomtest.model.User
import com.binar.roomtest.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.custom_rv.*
import kotlinx.android.synthetic.main.fragment_update.*

class UpdateFragment : DialogFragment() {
    private var _binding: FragmentUpdateBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<UpdateFragmentArgs>()
    private lateinit var mUserViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUpdateBinding.inflate(inflater, container, false)
        val view = binding.root

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        binding.updateNama.setText(args.currentUser.judul)
        binding.updateEmail.setText(args.currentUser.catatan)

        binding.btnUpdate.setOnClickListener {
            updateItem()
        }

        return view
    }

    private fun updateItem(){
        val judul = updateNama.text.toString()
        val catatan = updateEmail.text.toString()

        if(inputCheck(judul, catatan)){
            val updatedUser = User(args.currentUser.id, judul, catatan)
            mUserViewModel.updateUser(updatedUser)
            Toast.makeText(requireContext(), "Data berhasil di update", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }
        else{
            Toast.makeText(requireContext(), "Data belum di isi", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck (Judul: String, Catatan: String): Boolean{
        return !(TextUtils.isEmpty(Judul) && TextUtils.isEmpty(Catatan))
    }
}