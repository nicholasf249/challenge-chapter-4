package com.binar.roomtest

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.navigation.findNavController
import com.binar.roomtest.databinding.ActivityLoginBinding
import kotlin.math.log

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val sharedPreferences = "LOGIN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)

        val login = sharedPreferences.getBoolean("LOGIN", false)

        if (login == true){
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(intent)
        }

        binding.tvRegister.setOnClickListener {
            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        binding.btnLogin.setOnClickListener {
            val logEmail = sharedPreferences.getString("EMAIL", null)
            val logPass = sharedPreferences.getString("PASS", null)

            if (logEmail.equals(binding.loginEmail.text.toString()) && logPass.equals(binding.loginPass.text.toString())){
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putBoolean("LOGIN", true)
                editor.apply()

                Toast.makeText(this, "Anda berhasil login.", Toast.LENGTH_LONG).show()
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }

            else if ((logEmail == null) && (logPass == null)){
                Toast.makeText(this, "Data kosong", Toast.LENGTH_LONG).show()
            }

            else{
                Toast.makeText(this, "Data yang anda masukkan tidak sesuai", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Keluar Aplikasi")
            .setMessage("Apakah anda ingin keluar?")
            .setPositiveButton("Ya") { _, _ ->
                super.onBackPressed()
                finishAffinity()
            }
            .setNegativeButton("Tidak") { _, _ ->
            }
            .show()
    }
}