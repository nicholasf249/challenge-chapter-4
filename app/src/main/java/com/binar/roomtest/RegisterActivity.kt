package com.binar.roomtest

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.findNavController
import com.binar.roomtest.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val sharedPreferences = "LOGIN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)

        binding.btnRegister.setOnClickListener {
            val userName: String = binding.regUser.text.toString()
            val email: String = binding.regEmail.text.toString()
            val password: String = binding.regPass.text.toString()
            val confPass: String = binding.regConfPass.text.toString()

            if (binding.regPass.text.toString() != binding.regConfPass.text.toString()){
                Toast.makeText(this, "Konfirmasi password anda tidak sesuai!", Toast.LENGTH_LONG).show()
                binding.regPass.text.clear()
                binding.regConfPass.text.clear()
            }

            else{
                val editor: SharedPreferences.Editor = sharedPreferences.edit()

                editor.putString("USER", userName)
                editor.putString("EMAIL", email)
                editor.putString("PASS", password)
                editor.putString("PASSAUTH", confPass)
                editor.apply()
                Toast.makeText(this@RegisterActivity, "Akun berhasil dibuat.", Toast.LENGTH_LONG).show()

                val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                startActivity(intent)
            }
        }

    }
}